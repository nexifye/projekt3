using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingsMenu : MonoBehaviour
{
    // Skrypt na zaladowanie sceny z gr�
    public void Game()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("projekt3");
    }

    public void SetQuality()
    {
        SetScreenResolution();
    }

    void SetScreenResolution()
    {
        string index = UnityEngine.EventSystems.EventSystem.current.currentSelectedGameObject.name;

        switch (index)
        {
            case "0":
                Screen.SetResolution(1920, 1080, true);
                Debug.Log("Resolution changed to 1920x1080.");
                break;
            case "1":
                Screen.SetResolution(1280, 720, true);
                Debug.Log("Resolution changed to 1280x720.");
                break;
            case "2":
                Screen.SetResolution(720, 480, true);
                Debug.Log("Resolution changed to 720x480.");
                break;
        }
    }
}
