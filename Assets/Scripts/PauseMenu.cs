using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour
{
    // Zmienne
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI;

    // Skrypt by gra nie uruchomila sie z wlaczonym PauseMenu
    void Start()
    {
        pauseMenuUI.SetActive(false);
    }

    // Przypisanie klawiszy
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
        }

    // Restart
        if (Input.GetKeyDown(KeyCode.F5))
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene("projekt3");
        }
    }

    // Skrypt na wznowienie gry
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

    }
    // Skrypt na pauze

    void Pause()
    {
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    // Skrypt na zaladowanie sceny z SettingsMenu
    public void OptionsMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("settingsMenu");
    }
    // Skrypt na zaladowanie sceny z Menu
    public void MainMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
    }

    // Skrypt na zamkniecie gry
    public void QuitGame()
    {
        Debug.Log("Forcing quit.");
        Application.Quit();
    }
}
