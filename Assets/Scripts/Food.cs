using UnityEngine;

public class Food : MonoBehaviour
{
    // Zmienna
    public BoxCollider2D foodPlacement;
    public int points;

    // Randomowe pozycje jedzenia
    private void Start()
    {
        RandomizePosition();
    }

    private void RandomizePosition()
    {
        Bounds bounds = this.foodPlacement.bounds;

        float x = Random.Range(bounds.min.x, bounds.max.x);
        float y = Random.Range(bounds.min.y, bounds.max.y);

        this.transform.position = new Vector3(Mathf.Round(x), Mathf.Round(y), 0.0f);
    }

    // Wyzwalacz
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player") {
            RandomizePosition();
        }
    }
}
