using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    // Odpalenie sceny z gra bedac w Menu
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Menu z ustawieniami
    public void OptionsMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("optionsMenu");
    }

    // Wyjdz z gry
    public void QuitGame()
    {
        Debug.Log("Forcing quit.");
        Application.Quit();
    }
}
